import bs4
import html
import requests
import re

from typing import List

from offer import Offer


URL: str = "https://gog.com"
offers: List[Offer] = []


if __name__ == "__main__":
    r: requests.Response = requests.get(
        f"{URL}/work", cookies={"gog_lc": "PL_PLN_en-US"}
    )
    soup: bs4.BeautifulSoup = bs4.BeautifulSoup(r.content, "html.parser")

    offer_divs: bs4.ResultSet = soup.find_all(
        "a", {"class": "offer-wrapper"}
    )

    for offer_d in offer_divs:
        offer_class = offer_d.find_parent("div", {"class": "department-job-offers"})
        category_title_tag: bs4.Tag = offer_class.findChild(
            "div", {"class": "department-description-title"}
        )
        category: str = html.unescape(
            re.sub(r"\ \(\{\{.*$", "", category_title_tag.text.strip())
        )

        name_tag: bs4.Tag = offer_d.findChild("div", {"class": "offer-title"})
        name: str = html.unescape(name_tag.find(text=True, recursive=False).strip())

        offer: Offer = Offer(category, name, URL + offer_d["href"])

        offer.pull_info()
        offer.dump_to_db()

        offers.append(offer)
