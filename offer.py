import bs4
import re
import requests
import sqlite3

from typing import Optional


DB_NAME: str = "gog_offers.db"


class Offer:
    """Offer class that holds information, then transfered to a DB.

    Attributes:
        _db_initiated: Flag that shows if DB tables were initiated.
        _db_conn: SQLite3 connection shared between instances,
            single connection, to avoid DB locking with multiple
            connections in a simple way.
        category: Categfory/Department associated with the offer.
        name: Offer title.
        url: URL of the 'details' offer page.
        _info_pulled: Flag that indicates if detailed info was pulled.
        tags: List of tags associated with the offer.
        description: Description of the offer.
        responsibilities: Responsibilities associated with the offer.
        requirements: Requirements associated with the offer.
        pluses: Pluses associated with the offer.
        additional_benefits: Additional_benefits associated with the offer.
        offer_id: ID of the offer in SQLite DB.
    """
    _db_initiated: bool = False
    _db_conn: sqlite3.Connection = sqlite3.connect(DB_NAME)

    def __init__(self, category: str, name: str, url: str):
        """Initializing Offer object with minimal needed ammount of data.
        Optionaly initializes DB tables.

        Args:
            category: Categfory/Department associated with the offer.,
            name: Offer title.
            url: URL of the 'details' offer page.
        """

        # Initial fields
        self.category: str = category
        self.name: str = name
        self.url: str = url

        # Info fields
        self._info_pulled: bool = False
        self.tags: list = []
        self.description: str = ""
        self.responsibilities: list = []
        self.requirements: list = []
        self.pluses: list = []
        self.additional_benefits: list = []
        self.offer_id: Optional[int] = None

        # Internal properties
        self._LISTS_MAPPING: dict = {
            "Responsibilities:": self.responsibilities,
            "Job Requirements:": self.requirements,
            "Pluses:": self.pluses,
            "What we offer:": self.additional_benefits
        }
        if not self._db_initiated:
            self._init_tables()
            self._db_initiated = True

    def _init_tables(self):
        """Creates tables used to represent offers in SQLite."""

        # Data tables
        self._db_conn.execute("""
            create table if not exists categories(
                id integer primary key autoincrement,
                category text not null,
                unique(category)
            );
        """)
        self._db_conn.execute("""
            create table if not exists tags(
                id integer primary key autoincrement,
                tag text not null,
                unique(tag)
            );
        """)
        self._db_conn.execute("""
            create table if not exists responsibilities(
                id integer primary key autoincrement,
                resp_text text not null,
                unique(resp_text)
            );
        """)
        self._db_conn.execute("""
            create table if not exists requirements(
                id integer primary key autoincrement,
                requirement text not null,
                unique(requirement)
            );
        """)
        self._db_conn.execute("""
            create table if not exists pluses(
                id integer primary key autoincrement,
                plus text not null,
                unique(plus)
            );
        """)
        self._db_conn.execute("""
            create table if not exists additional_benefits(
                id integer primary key autoincrement,
                additional_benefit text not null,
                unique(additional_benefit)
            );
        """)
        self._db_conn.execute("""
            create table if not exists offers(
                id integer primary key autoincrement,
                name text not null,
                url text not null,
                description text not null,
                unique(name, url)
            );
        """)

        # MtM relatiopn tables
        self._db_conn.execute("""
            create table if not exists offer_to_category(
                id integer primary key autoincrement,
                offer_id integer,
                category_id integer,
                foreign key (offer_id)
                    references offers(id),
                foreign key (category_id)
                    references categories(id)
            );
        """)
        self._db_conn.execute("""
            create table if not exists offer_to_tag(
                id integer primary key autoincrement,
                offer_id integer,
                tag_id integer,
                foreign key (offer_id)
                    references offers(id),
                foreign key (tag_id)
                    references tags(id)
            );
        """)
        self._db_conn.execute("""
            create table if not exists offer_to_responsibility(
                id integer primary key autoincrement,
                offer_id integer,
                responsibility_id integer,
                foreign key (offer_id)
                    references offers(id),
                foreign key (responsibility_id)
                    references responsibilities(id)
            );
        """)
        self._db_conn.execute("""
            create table if not exists offer_to_requirement(
                id integer primary key autoincrement,
                offer_id integer,
                requirement_id integer,
                foreign key (offer_id)
                    references offers(id),
                foreign key (requirement_id)
                    references requirements(id)
            );
        """)
        self._db_conn.execute("""
            create table if not exists offer_to_plus(
                id integer primary key autoincrement,
                offer_id integer,
                plus_id integer,
                foreign key (offer_id)
                    references offers(id),
                foreign key (plus_id)
                    references pluses(id)
            );
        """)
        self._db_conn.execute("""
            create table if not exists offer_to_additional_benefit(
                id integer primary key autoincrement,
                offer_id integer,
                additional_benefit_id integer,
                foreign key (offer_id)
                    references offers(id),
                foreign key (additional_benefit_id)
                    references additional_benefits(id)
            );
        """)

    def __del__(self):
        """Close connection to DB."""
        self._db_conn.close()

    def __str__(self):
        return f"Offer {self.name} ({self.category}) at {self.url}"

    def __repr__(self):
        return "Offer object [{}] - {} ({}) at {}".format(
            hex(id(self)), self.name, self.category, self.url
        )

    def pull_info(self):
        """Pulls detailed information about the offer, and fills out the
        fields of the instance accordingly.
        """

        offer_r: requests.Response = requests.get(
            self.url, cookies={"gog_lc": "PL_PLN_en-US"}
        )
        offer_soup: bs4.BeautifulSoup = bs4.BeautifulSoup(
            offer_r.content, "html.parser"
        )

        # Get offer tags
        for t in offer_soup.find_all("a", {"class": "offer-tag"}):
            self.tags.append(
                re.sub(r"^#", "", t.get_text().strip())
            )

        # Get offer description text
        self.description: str = offer_soup \
            .find("div", {"class": "job-offer-description"}) \
            .findChild("p").get_text().strip()

        # Get info lists
        detail_lists: bs4.ResultSet = offer_soup \
            .find("div", {"class": "job-offer-wrapper"}) \
            .find_all("ul")
        for dd in detail_lists:
            heading: str = dd.parent.previous_sibling.get_text().strip()
            for item in dd.children:
                self._LISTS_MAPPING[heading].append(
                    item.get_text().strip()
                )

        self._info_pulled = True

    def dump_to_db(self):
        """Dump data fields of the instance to the DB."""

        cursor: sqlite3.Cursor = self._db_conn.cursor()

        # Offer insertion
        cursor.execute("""
            select id from offers where name=? and url=?;
                       """, (self.name, self.url))
        try:
            self.offer_id = cursor.fetchone()[0]
        except TypeError:
            self.offer_id = None

        if self.offer_id is None:
            cursor.execute("""
                insert into offers
                    (name, url, description)
                values
                           (?, ?, ?);
            """, (self.name, self.url, self.description))
            self._db_conn.commit()
            self.offer_id = cursor.lastrowid

        # Category insertion & connection
        cursor.execute("""
            select id from categories where category=?;
                       """, (self.category,))
        try:
            category_id = cursor.fetchone()[0]
        except TypeError:
            category_id = None

        if category_id is None:
            cursor.execute("""
                insert into categories
                    (category)
                values
                           (?);
            """, (self.category,))
            self._db_conn.commit()
            category_id = cursor.lastrowid

        cursor.execute("""
            insert or ignore into offer_to_category
                (offer_id, category_id)
            values
                (?, ?)
        """, (self.offer_id, category_id))
        self._db_conn.commit()

        self._connect_to_offer(
            self.tags, "tags", "tag", "offer_to_tag", "tag_id"
        )
        self._connect_to_offer(
            self.responsibilities, "responsibilities", "resp_text",
            "offer_to_responsibility", "responsibility_id"
        )
        self._connect_to_offer(
            self.requirements, "requirements", "requirement",
            "offer_to_requirement", "requirement_id"
        )
        self._connect_to_offer(
            self.pluses, "pluses", "plus", "offer_to_plus", "plus_id"
        )
        self._connect_to_offer(
            self.additional_benefits, "additional_benefits",
            "additional_benefit", "offer_to_additional_benefit",
            "additional_benefit_id"
        )

    def _connect_to_offer(
        self, item_list: list, item_table_name: str,
        item_field_name: str, connection_table_name: str,
        connection_field_name: str
    ):
        """Insert items that haven't been inserted yet and create MtM connections."""

        if self.offer_id is not None:
            cursor = self._db_conn.cursor()
            for item in item_list:
                cursor.execute(f"""
                    select id from {item_table_name} where {item_field_name}=?;
                            """, (item, ))
                try:
                    item_id: Optional[int] = cursor.fetchone()[0]
                except TypeError:
                    item_id = None

                if item_id is None:
                    cursor.execute(f"""
                        insert into {item_table_name}
                            ({item_field_name})
                        values
                            (?);
                    """, (item, ))
                    self._db_conn.commit()
                    item_id = cursor.lastrowid

                cursor.execute(f"""
                    insert or ignore into {connection_table_name}
                        (offer_id, {connection_field_name})
                    values
                        (?, ?)
                """, (self.offer_id, item_id))
                self._db_conn.commit()
